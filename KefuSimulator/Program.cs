﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace KefuSimulator
{
    class Program
    {
        [DataContract]
        public class RobotRecognition
        {
            [DataMember(Name = "name")]
            public int Id
            {
                get;
                set;
            }
            [DataMember(Name = "position")]
            public double[] Position
            {
                get;
                set;
            }
            [DataMember(Name = "orientation")]
            public double Orientation
            {
                get;
                set;
            }
        }

        private RobotRecognition robot;
        private short leftMotor, rightMotor;

        async void UdpThread()
        {
            var client = new UdpClient();
            var ep = new IPEndPoint(IPAddress.Loopback, 12345);

            while (true)
            {
                var se = new DataContractJsonSerializer(typeof(RobotRecognition));
                using (var ms = new MemoryStream())
                {
                    se.WriteObject(ms, robot);
                    using (var sr = new StreamReader(ms))
                    {
                        var s = "[" + sr.ReadToEnd() + "]";
                        var bytes = Encoding.UTF8.GetBytes(s);
                        await client.SendAsync(bytes, bytes.Length, ep);
                    }
                }
                await Task.Delay(66);
            }
        }

        void Main()
        {
            var portName = "COM10";
            var deviceName = "VirtualNXT0";
            var serial = new SerialPort(portName, 9600);
            robot = new RobotRecognition();
            robot.Position = new double[2];

            Console.WriteLine("Open port.");
            serial.Open();

            UdpThread();
            while (true)
            {
                var buffer = new byte[4];
                serial.Read(buffer, 0, 4);
                if (buffer[0] == 0xA0 && buffer[1] == 0xA0 &&
                    buffer[2] == 0xA0 && buffer[3] == 0xA0)
                {
                    var data = new byte[32];
                    Encoding.UTF8.GetBytes(deviceName).CopyTo(data, 0);
                    serial.Write(data, 0, 32);
                }
                else
                {
                    int left = (buffer[0] | (((ushort)buffer[1]) << 8));
                    int right = (buffer[2] | (((ushort)buffer[3]) << 8));
                    if (left > 0x7fff) left -= 0x8000;
                    if (right > 0x7fff) right -= 0x8000;
                    leftMotor = (short)left;
                    rightMotor = (short)right;
                }
            }
        }

        static void Main(string[] args)
        {
            var self = new Program();
            self.Main();
        }
    }
}
