#define WIN32_LEAN_AND_MEAN
#include<Windows.h>
#include<WinSock2.h>

#include<sstream>
#include<cmath>

#pragma comment(lib, "ws2_32.lib")

#define PI (3.14159265358979)
// ここを外すとシミュレーションモード
//#define SIMULATION 1

#pragma pack(push,1)

struct KefuVector
{
	double x;
	double y;
};

struct KefuPosition
{
	double x;
	double y;
};

struct KefuRobot
{
	char id;
	KefuPosition position;
	KefuVector orientation;
};

#pragma pack(pop)

#if !defined(SIMULATION)
bool QueryCurrentFrame(KefuRobot* robots, int* size)
{
	return true;
}
#else
double x = 0.5, y = 0.5, vx = 0.01, vy= 0.02;

bool QueryCurrentFrame(KefuRobot* robots, int* size)
{
	if(!robots){
		*size = 1;
		return true;
	}
	robots[0].id = 0;
	robots[0].position.x = x;
	robots[0].position.y = y;
	robots[0].orientation.x = vx;
	robots[0].orientation.y = vy;
	if(x + vx > 1){ vx *= -1; }
	if(x + vx < 0){ vx *= -1; }
	if(y + vy > 1){ vy *= -1; }
	if(y + vy < 0){ vy *= -1; }
	x += vx;
	y += vy;
	return true;
}
#endif

int main()
{
	WSAData wsa;
	WSAStartup(MAKEWORD(2,0), &wsa);

	SOCKET sock;
	struct sockaddr_in addr;

	sock = socket(AF_INET, SOCK_DGRAM, 0);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(12345);
	addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");

	while(1)
	{
		// 個数を調べる
		int size = 0;
		QueryCurrentFrame(nullptr, &size);
		
		// 実際に要求する
		KefuRobot* robots = new KefuRobot[size];
		QueryCurrentFrame(robots, &size);
		
		// JSONに変換して渡してあげる
		std::stringstream ss;
		ss << "[";
		for(int i = 0; i < size; ++i)
		{
			auto a = robots[i].orientation.x;
            auto b = robots[i].orientation.y;
            auto h = sqrt(pow(a, 2) + pow(b, 2));
            auto t = asin(a / h);
            if (b > 0) t = PI - t;
            auto angle = t / PI * 180;
            if (angle < 0) angle += 360;
			if(i) ss << ",";
			ss << "{";
			ss << "\"id\":" << (int)robots[i].id << ",";
			ss << "\"position\":[" << robots[i].position.x << "," << robots[i].position.y << "],";
			ss << "\"orientation\":" << angle << "}";
		}
		ss << "]";

		// UDPで転送！
		auto str = ss.str();
		sendto(sock, str.c_str(), str.size(), 0, (struct sockaddr *)&addr, sizeof(addr));

		delete[] robots;
		Sleep(1 * 1000);
	}
}
