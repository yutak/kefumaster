﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using OpenCvSharp;
using Point = System.Windows.Point;

namespace KefuMaster
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<RobotViewModel> robots;

        private Point oldPoint;

        private CvCapture capture;

        public int ThGray { get; set; }
        public int ThRed { get; set; }
        public int ThBlue { get; set; }
        public int ArCorner { get; set; }
        public int ArMarker { get; set; }

        public string TargetPosition { get; set; }
        public bool IsPlaying { get; set; }

        public SenarioParser Senario { get; set; }
        public SceneParser Scene { get; set; }

        enum Mode
        {
            Gray,
            Red,
            Green,
            Blue,
            Binaly,
            Color,
            Parspective
        }

        private Mode mode;

        [DataContract]
        public class RobotRecognition
        {
            [DataMember(Name = "id")]
            public int Id
            {
                get;
                set;
            }
            [DataMember(Name = "position")]
            public double[] Position
            {
                get;
                set;
            }
            [DataMember(Name = "orientation")]
            public double Orientation
            {
                get;
                set;
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            var r = new Random();

#if STANDALONE
            robots = new ObservableCollection<RobotViewModel>(Enumerable.Range(0, 1).Select(i => new RobotViewModel
            {
                Id = i + 1,
                LeftMoter = r.NextDouble() * 2 - 1,
                RightMoter = r.NextDouble() * 2 - 1,
                X = r.Next(400),
                Y = r.Next(300),
                Direction = r.Next(360)
            }));
            foreach (var robot in robots)
            {
                robot.Nearby = new NearbyRobotsEnumerable(robot.Id, robots);
            }
            Robots.ItemsSource = robots;
#endif

            Scene = new SceneParser("Scene.txt");

            DataContext = this;
            ThGray = 65;
            ThRed = 85;
            ThBlue = 80;
            ArCorner = 200;
            ArMarker = 1000;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
#if STANDALONE
            return;

            capture = new CvCapture(CaptureDevice.Any);
            capture.FrameWidth = 640;
            capture.FrameHeight = 480;

            (new Thread(() =>
            {
                while (true)
                {
                    IplImage frame;
                    lock (capture)
                    {
                        if (capture.IsDisposed)
                            break;
                        frame = capture.QueryFrame();
                    }

                    var color = new IplImage(frame.Size, BitDepth.U8, 3);
                    var gray = new IplImage(frame.Size, BitDepth.U8, 1);
                    var red = new IplImage(frame.Size, BitDepth.U8, 1);
                    var red_B = new IplImage(frame.Size, BitDepth.U8, 1);
                    var green = new IplImage(frame.Size, BitDepth.U8, 1);
                    var blue = new IplImage(frame.Size, BitDepth.U8, 1);
                    var blue_B = new IplImage(frame.Size, BitDepth.U8, 1);
                    var tmp = new IplImage(frame.Size, BitDepth.U8, 1);
                    var stage = new IplImage(frame.Size, BitDepth.U8, 1);
                    var transform = new IplImage(frame.Size, BitDepth.U8, 3);
                    var storage = new CvMemStorage();
                    CvSeq<CvPoint> contours;
                    frame.Copy(color);
                    frame.CvtColor(gray, ColorConversion.BgrToGray);
                    gray.Threshold(stage, ThGray, 255, ThresholdType.BinaryInv);
                    gray.Threshold(tmp, ThGray, 255, ThresholdType.BinaryInv);
                    tmp.FindContours(storage, out contours, CvContour.SizeOf, ContourRetrieval.List, ContourChain.ApproxSimple);

                    var corners = new List<CvPoint2D32f>();

                    while (contours != null)
                    {
                        var result = Cv.ApproxPoly(contours, CvContour.SizeOf, storage, ApproxPolyMethod.DP, Cv.ContourPerimeter(contours) * 0.02, false);
                        {
                            var area = Cv.ContourArea(contours, CvSlice.WholeSeq);
                            if (Math.Abs(area) > ArCorner)
                            {
                                //if (Cv.CheckContourConvexity(result))
                                {
                                    var vtx = result.Select(point => point.Value).OrderBy(point => point.X + point.Y).ToList();
                                    var min = vtx.First();
                                    var max = vtx.Last();
                                    color.Rectangle(min, max, CvColor.Red, 2);
                                    corners.Add(new CvPoint2D32f((max.X + min.X) / 2.0, (max.Y + min.Y) / 2.0));
                                }
                            }
                        }
                        contours = contours.HNext;
                    }

                    if (corners.Count == 4)
                    {
                        var parspective = new[]
                        {
                            new CvPoint2D32f(0, 0),
                            new CvPoint2D32f(480, 0),
                            new CvPoint2D32f(0, 480),
                            new CvPoint2D32f(480, 480)
                        };

                        var cx = corners.Select(f => f.X).Aggregate(0.0, (i, f) => i + f) / 4.0;
                        var cy = corners.Select(f => f.Y).Aggregate(0.0, (i, f) => i + f) / 4.0;
                        try
                        {
                            var from = new[]
                            {
                                corners.First(f => f.X < cx && f.Y < cy),
                                corners.First(f => f.X > cx && f.Y < cy),
                                corners.First(f => f.X < cx && f.Y > cy),
                                corners.First(f => f.X > cx && f.Y > cy)
                            };

                            var matrix = Cv.GetPerspectiveTransform(from, parspective);
                            frame.WarpPerspective(transform, matrix, Interpolation.Linear | Interpolation.FillOutliers, CvScalar.ScalarAll(100));
                        }
                        catch
                        {

                        }
                    }

                    transform.Split(blue, green, red, null);
                    red.Threshold(red, ThRed, 255, ThresholdType.Binary);
                    blue.Threshold(blue, ThBlue, 255, ThresholdType.BinaryInv);
                    red.And(blue, tmp);

                    tmp.FindContours(storage, out contours, CvContour.SizeOf, ContourRetrieval.List, ContourChain.ApproxSimple);

                    CvPoint2D32f? pt = null;

                    while (contours != null)
                    {
                        var result = Cv.ApproxPoly(contours, CvContour.SizeOf, storage, ApproxPolyMethod.DP, Cv.ContourPerimeter(contours) * 0.02, false);
                        {
                            var area = Cv.ContourArea(contours, CvSlice.WholeSeq);
                            if (Math.Abs(area) > ArMarker)
                            {
                                //if (Cv.CheckContourConvexity(result))
                                {
                                    var vtx = result.Select(point => point.Value).OrderBy(point => point.X + point.Y).ToList();
                                    var min = vtx.First();
                                    var max = vtx.Last();
                                    transform.Rectangle(min, max, CvColor.Blue, 2);
                                    pt = new CvPoint2D32f((max.X + min.X) / 2.0, (max.Y + min.Y) / 2.0);
                                }
                            }
                        }
                        contours = contours.HNext;
                    }

                    var ms = new MemoryStream();
                    switch (mode)
                    {
                        case Mode.Color:
                            color.ToStream(ms, ".png");
                            break;
                        case Mode.Gray:
                            gray.ToStream(ms, ".png");
                            break;
                        case Mode.Red:
                            red.ToStream(ms, ".png");
                            break;
                        case Mode.Green:
                            green.ToStream(ms, ".png");
                            break;
                        case Mode.Blue:
                            blue.ToStream(ms, ".png");
                            break;
                        case Mode.Binaly:
                            stage.ToStream(ms, ".png");
                            break;
                        case Mode.Parspective:
                            transform.ToStream(ms, ".png");
                            break;
                    }

                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.EndInit();
                        hogeImage.Source = bmp;
                        if (pt.HasValue)
                        {
                            var a = pt.Value.X - robots[0].X;
                            var b = pt.Value.Y - robots[0].Y;
                            var h = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
                            var t = Math.Asin(a / h);
                            if (b > 0) t = Math.PI - t;
                            var angle = t / Math.PI * 180;
                            if (angle < 0) angle += 360;
                            angle %= 360;
                            robots[0].X = pt.Value.X;
                            robots[0].Y = pt.Value.Y;
                            robots[0].Direction = angle;
                        }
                    }));
                    Thread.Sleep(66);
                }
            })).Start();
#endif
        }

        private void Path_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(sender as UIElement);
            oldPoint = e.GetPosition(this);
        }

        private void Path_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed) return;
            var pt = e.GetPosition(this);
            ((sender as FrameworkElement).DataContext as RobotViewModel).X += pt.X - oldPoint.X;
            ((sender as FrameworkElement).DataContext as RobotViewModel).Y += pt.Y - oldPoint.Y;
            oldPoint = pt;
        }

        private void Path_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Mouse.Capture(null);
        }

        private void Path_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            ((sender as FrameworkElement).DataContext as RobotViewModel).Direction += e.Delta / 10.0;
        }

        #region Camera control event handler
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mode = Mode.Gray;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            mode = Mode.Red;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            mode = Mode.Green;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            mode = Mode.Blue;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            mode = Mode.Binaly;
        }
        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (capture == null) return;
            lock (capture) capture.Dispose();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            mode = Mode.Color;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            mode = Mode.Parspective;
        }

        // 利用可能なNXTを接続する
        private void SetupClick(object sender, RoutedEventArgs e)
        {
            var r = new Random();

            if (Properties.Settings.Default.Simulation)
            {
                robots = new ObservableCollection<RobotViewModel>(Enumerable.Range(0, 1).Select((port, i) => new RobotViewModel
                {
                    Id = i + 1,
                    LeftMoter = r.NextDouble() * 2 - 1,
                    RightMoter = r.NextDouble() * 2 - 1,
                    X = r.Next(400),
                    Y = r.Next(300),
                    Direction = r.Next(360)
                }));
                robots[0].Transport = new SerialPort("COM4", 9600);
                robots[0].Transport.ReadTimeout = 5000;
                robots[0].Transport.WriteTimeout = 5000;
                robots[0].Transport.Open();
            }
            else
            {
                if (robots != null)
                {
                    foreach (var ro in robots)
                    {
                        if (ro.Transport != null)
                        {
                            ro.Transport.Close();
                            ro.Transport.Dispose();
                            ro.Transport = null;
                        }
                    }
                }
                robots = new ObservableCollection<RobotViewModel>(GetNxtSerialPorts().Select((port, i) => new RobotViewModel
                {
                    Id = i + 1,
                    LeftMoter = r.NextDouble() * 2 - 1,
                    RightMoter = r.NextDouble() * 2 - 1,
                    X = r.Next(400),
                    Y = r.Next(300),
                    Direction = r.Next(360),
                    Transport = port
                }));
            }
            foreach (var robot in robots)
            {
                robot.Nearby = new NearbyRobotsEnumerable(robot.Id, robots);
            }
            Robots.ItemsSource = robots;
        }

        private async void StartClick(object sender, RoutedEventArgs e)
        {
            var suspendFrames = 0;
            var n = 0;
            List<IEnumerable<SenarioParserV1.Senario>> staticSenario = null;
            while (true)
            {
                // ロボットの位置情報を更新
                foreach (var robo in (await ReceivePacketsAsync()))
                {
                    if (robo.Id >= robots.Count) continue;
                    robots[robo.Id].Direction = robo.Orientation;
                    robots[robo.Id].X = robo.Position[0] * 518;
                    robots[robo.Id].Y = robo.Position[1] * 508;
                }

                if (IsPlaying && Senario != null)
                {
                    if (Senario.Version == SenarioParser.SenarioVersion.V2)
                    {
                        // V1シナリオを削除
                        staticSenario = null;
                        // シナリオに合わせて制御してるとこ
                        lock (Senario)
                        {
                            var senarioV2 = Senario as SenarioParserV2;
                            ControlRobotsWithDynamicSenario(senarioV2);
                        }
                    }
                    else
                    {
                        var senarioV1 = Senario as SenarioParserV1;
                        if (staticSenario == null)
                        {
                            staticSenario = new List<IEnumerable<SenarioParserV1.Senario>>(senarioV1);
                        }
                        var sen = staticSenario.FirstOrDefault();
                        if (sen != null)
                        {
                            ControlRobotsWithStaticSenario(sen);
                            staticSenario.RemoveAt(0);
                        }
                        if (staticSenario.Count == 0)
                        {
                            staticSenario = null;
                            IsPlaying = false;
                        }
                    }
                }
                // 10回に1回画面を更新する
                // たぶん間に合うはず…？
                if (n++ > suspendFrames)
                {

                    var last = DateTime.Now;
                    foreach (var r in robots)
                    {
                        r.Raise();
                    }
                    n = 0;
                    var millis = (last - DateTime.Now).TotalMilliseconds;
                    if (millis > 100)
                    {
                        suspendFrames = (int)(millis / 100);
                    }
                }
                await Task.Delay(100);
            }
        }

        private void ControlRobotsWithDynamicSenario(SenarioParserV2 senarioV2)
        {
            foreach (var m in robots.Join(senarioV2, x => x.Id - 1, x => x.Id, (x, y) => new { Model = x, Point = y.Point }))
            {
                if (Math.Abs(m.Point.X - m.Model.X) < 10 && Math.Abs(m.Point.Y - m.Model.Y) < 10)
                {
                    char command = 'C';
                    // 到着時の方向修正
                    if (m.Point.Angle >= 0)
                    {
                        var ta = m.Point.Angle - m.Model.Direction;
                        if (ta < 0) ta += 360;
                        if (ta > 20 && ta < 340)
                            command = ta < 180 ? 'L' : 'H';
                    }
                    m.Model.Transport.Write(new[] { (byte)command }, 0, 1);
                }
                else
                {
                    var ta = calculateDirection(m.Point, new Point(m.Model.X,m.Model.Y), m.Model.Direction);
                    if (ta < 20 || ta > 340)
                    {
                        // 前方±60
                        var highPriorityRobots = robots.Select((mo, i) => new {Id = i, Model = mo}).Where(r => r.Id < m.Model.Id);
                        var avoid = highPriorityRobots.Any(r =>
                        {
                            var ang = calculateDirection(new Point(r.Model.X, r.Model.Y), new Point(m.Model.X, m.Model.Y),
                                m.Model.Direction);
                            var dist = calculateDistance(new Point(r.Model.X, r.Model.Y),
                                new Point(m.Model.X, m.Model.Y));
                            if (ang - 300 > 0) ang = 360 - ang;
                            else ang = 60 - ang;
                            if (ang > 60 || ang < 0) return false;
                            // 閾値てきとうに。
                            // 前方10cm,左右60°へ広がるにつれて、Max20cmの範囲内に自身より優先度の高い
                            // 機体が存在すれば、動作を停止する。
                            return dist < (10 + 10*ang/60.0);
                        });
                        avoid = false;
                        // 衝突回避
                        m.Model.Transport.Write(avoid ? new[] {(byte) 'C'} : new[] {(byte) 'J'}, 0, 1);
                    }
                    else
                    {
                        // 方向修正
                        m.Model.Transport.Write(new[] { (byte)(ta < 180 ? 'L' : 'H') }, 0, 1);
                    }
                }
            }
        }

        private double calculateDirection(Point target, Point current, double direction)
        {
            var dx = target.X - current.X;
            var dy = target.Y - current.Y;
            double angle;
            if (dx == 0)
            {
                angle = dx < 0 ? 0 : 180;
            }
            else
            {
                angle = Math.Atan2(-dy, dx) * 180 / Math.PI;
                angle -= 90;
                if (angle < 0) angle += 360;
                angle = 360 - angle;
            }
            var ta = angle - direction;
            if (ta < 0) ta += 360;
            return ta;
        }

        private double calculateDistance(Point target, Point current)
        {
            return Math.Sqrt(Math.Pow(current.X - target.X, 2) + Math.Pow(current.Y - target.Y, 2));
        }

        private void ControlRobotsWithStaticSenario(IEnumerable<SenarioParserV1.Senario> sen)
        {
            foreach (var t in robots.Zip(sen, Tuple.Create))
            {
                switch (t.Item2)
                {
                    case SenarioParserV1.Senario.Forward:
                        t.Item1.Transport.Write(new[] { (byte)'J' }, 0, 1);
                        break;
                    case SenarioParserV1.Senario.Backward:
                        t.Item1.Transport.Write(new[] { (byte)'K' }, 0, 1);
                        break;
                    case SenarioParserV1.Senario.LeftTurn:
                        t.Item1.Transport.Write(new[] { (byte)'H' }, 0, 1);
                        break;
                    case SenarioParserV1.Senario.RightTurn:
                        t.Item1.Transport.Write(new[] { (byte)'L' }, 0, 1);
                        break;
                    case SenarioParserV1.Senario.Stop:
                        t.Item1.Transport.Write(new[] { (byte)'C' }, 0, 1);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        // 接続可能なNEX向けSerialPortの配列を取得する関数
        private IEnumerable<SerialPort> GetNxtSerialPorts()
        {
            if (Properties.Settings.Default.Simulation) yield break;
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.ComPort))
            {
                var sp = new SerialPort(Properties.Settings.Default.ComPort, BaudRate);
                sp.Open();
                yield return sp;
            }
            var id = 1;
            foreach (var port in SerialPort.GetPortNames())
            {
                var serial = new SerialPort(port, BaudRate);
                var magic = Encoding.UTF8.GetBytes("MINT");
                var buffer = new byte[32];
                try
                {
                    serial.Open();
                    serial.ReadTimeout = 5000;
                    serial.WriteTimeout = 5000;
                    serial.Write(magic, 0, 4);
                    serial.Read(buffer, 0, 4);
                    Console.WriteLine(serial.ToString());
                }
                catch
                {
                    continue;
                }
                var len = StrLen(buffer);
                if (len > 0)
                {
                    var s = Encoding.UTF8.GetString(buffer, 0, len);
                    if (s == "MINT")
                    {
                        serial.Write(new[] { (byte)id++ }, 0, 1);
                        yield return serial;
                    }
                    else
                    {
                        serial.Close();
                    }
                }
                else
                {
                    serial.Close();
                }
            }
        }

        // byte[]用strlen
        private static int StrLen(IEnumerable<byte> buffer)
        {
            return buffer.TakeWhile(b => b != 0).Count();
        }

        // UDPからロボットの位置を取得するメソッド
        private async Task<IEnumerable<RobotRecognition>> ReceivePacketsAsync()
        {
            var socket = new UdpClient(12345, AddressFamily.InterNetwork);
            var result = await socket.ReceiveAsync();
            socket.Close();
            var se = new DataContractJsonSerializer(typeof(RobotRecognition[]));
            RobotRecognition[] robots;
            using (var ms = new MemoryStream(result.Buffer))
            {
                robots = (RobotRecognition[])se.ReadObject(ms);
            }
            return robots;
        }

        public int BaudRate { get { return 9600; } }

        private void GotoTarget(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TargetPosition)) return;
            if (!TargetPosition.Contains(",")) return;
            var t = TargetPosition.Split(',');
            //TargetX = int.Parse(t[0]);
            //TargetY = int.Parse(t[1]);
            IsPlaying = true;
        }

        private void PlaySenario(object sender, RoutedEventArgs e)
        {
            if (SenarioSelector.SelectedIndex < 0) return;
            var s = Scene[SenarioSelector.SelectedIndex];
            if (Senario != null) lock (Senario) Senario = s;
            else Senario = s;
            IsPlaying = true;
        }

        private void EmergencyTerminate(object sender, RoutedEventArgs e)
        {
            if (IsPlaying && Senario != null)
            {
                IsPlaying = false;
                // シナリオに合わせて制御してるとこ
                lock (Senario)
                    foreach (var r in robots)
                    {
                        r.Transport.Write(new[] { (byte)'C' }, 0, 1);
                    }
            }
        }
    }

    public class NearbyRobotsEnumerable : IEnumerable<NearbyRobotViewModel>, INotifyCollectionChanged
    {
        private ObservableCollection<RobotViewModel> _linkedCollection;
        private int _myId;

        public NearbyRobotsEnumerable(int myId, ObservableCollection<RobotViewModel> linkedCollection)
        {
            _myId = myId;
            _linkedCollection = linkedCollection;
            _linkedCollection.CollectionChanged += _linkedCollection_CollectionChanged;
            foreach (var model in _linkedCollection)
            {
                model.PropertyChanged += model_PropertyChanged;
            }
        }

        void model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var h = CollectionChanged;
            if (h != null) h(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        void _linkedCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach (var oldItem in e.OldItems)
            {
                (oldItem as INotifyPropertyChanged).PropertyChanged -= model_PropertyChanged;
            }
            foreach (var newItem in e.NewItems)
            {
                (newItem as INotifyPropertyChanged).PropertyChanged += model_PropertyChanged;
            }
        }

        public IEnumerator<NearbyRobotViewModel> GetEnumerator()
        {
            var m = _linkedCollection.First(model => model.Id == _myId);
            foreach (var model in _linkedCollection.Where(model => model.Id != _myId))
            {
                var n = new NearbyRobotViewModel();
                var a = model.X - m.X;
                var b = model.Y - m.Y;
                var h = Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
                var t = Math.Asin(a / h);
                if (b > 0) t = Math.PI - t;
                n.Distance = h / 5;
                n.Angle = t / Math.PI * 180 - m.Direction;
                if (n.Angle < 0) n.Angle += 360;
                n.Angle %= 360;
                n.Id = model.Id;
                yield return n;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }

    public class RobotModel
    {

    }

    public class RobotViewModel : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public double LeftMoter { get; set; }
        public double RightMoter { get; set; }
        public double Direction { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public IEnumerable<NearbyRobotViewModel> Nearby { get; set; }

        public SerialPort Transport { get; set; }

        private T Raise<T>(T value, string prop = "")
        {
            var h = PropertyChanged;
            if (h != null) h(this, new PropertyChangedEventArgs(prop));
            return value;
        }

        public void Raise()
        {
            Raise("Direction");
            Raise("X");
            Raise("Y");
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class NearbyRobotModel
    {

    }

    public class NearbyRobotViewModel
    {
        public double Angle { get; set; }
        public double Distance { get; set; }
        public int Id { get; set; }
    }

    public class MoterPowerToOffsetConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var dir = (string)parameter;
            var pwr = (double)value;
            if (dir == "backward")
            {
                return pwr > 0 ? 0.5 : (0.5 - pwr * 0.5);
            }
            return pwr < 0 ? 0.5 : (0.5 - pwr * 0.5);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class NearbyConverter : IValueConverter
    {
        private const double ViewRadius = 20; // ロボットの視界(暫定cm)

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var robo = (NearbyRobotViewModel)value;
            var opt = (string)parameter;
            var pt = 32.0;
            if (opt == "top")
                pt += (pt * robo.Distance / ViewRadius * (-Math.Cos(robo.Angle / 180 * Math.PI)));
            else
                pt += (pt * robo.Distance / ViewRadius * (Math.Sin(robo.Angle / 180 * Math.PI)));
            return pt;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
