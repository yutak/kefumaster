﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KefuMaster
{
    public abstract class SenarioParser
    {
        public enum SenarioVersion
        {
            V1,
            V2,
        }

        public abstract SenarioVersion Version { get; }

        public static SenarioParser Parse(string fileName)
        {
            var version = SenarioVersion.V2;
            using (var fs = new FileStream(fileName, FileMode.Open))
            using (var sr = new StreamReader(fs))
            {
                var s = sr.ReadLine();
                if (s.StartsWith("# version:"))
                {
                    var v = int.Parse(s.Split(':')[1].Trim());
                    switch (v)
                    {
                        case 0:
                        case 2:
                            break;
                        case 1:
                            version = SenarioVersion.V1;
                            break;
                        default:
                            throw new Exception();
                    }
                }
            }
            return version == SenarioVersion.V2 ? (SenarioParser) new SenarioParserV2(fileName) : new SenarioParserV1(fileName);
        }
    }

    public class SenarioParserV1 : SenarioParser, IEnumerable<IEnumerable<SenarioParserV1.Senario>>
    {
        public enum Senario
        {
            Forward,
            Backward,
            LeftTurn,
            RightTurn,
            Stop
        }

        List<IEnumerable<Senario>> _senario;

        public SenarioParserV1(string fileName)
        {
            _senario = new List<IEnumerable<Senario>>();

            using (var fs = new FileStream(fileName, FileMode.Open))
            using (var sr = new StreamReader(fs))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith("#")) continue;
                    _senario.Add(s.Split('\t',',').Select(c =>
                    {
                        switch (c)
                        {
                            case "F":
                                return Senario.Forward;
                            case "B":
                                return Senario.Backward;
                            case "L":
                                return Senario.LeftTurn;
                            case "R":
                                return Senario.RightTurn;
                            case "C":
                                return Senario.Stop;
                            default:
                                throw new Exception();
                        }
                    }).ToArray());
                }
            }
        }

        public IEnumerator<IEnumerable<Senario>> GetEnumerator()
        {
            return _senario.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override SenarioVersion Version { get { return SenarioVersion.V1; } }
    }

    public class SenarioParserV2 : SenarioParser, IEnumerable<SenarioParserV2.Senario>
    {
        public class Point
        {
            public int X { get; set; }
            public int Y { get; set; }
            public double Angle { get; set; }

            public static implicit operator System.Windows.Point(Point pt)
            {
                return new System.Windows.Point(pt.X, pt.Y);
            }
        }

        public class Senario
        {
            public int Id { get; set; }
            public Point Point { get; set; }
        }

        List<Senario> _senario;

        public SenarioParserV2(string fileName)
        {
            _senario = new List<Senario>();
            using (var fs = new FileStream(fileName, FileMode.Open))
            using (var sr = new StreamReader(fs))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.StartsWith("#")) continue;
                    var cols = s.Split('\t').ToArray();
                    if (cols.Length < 2) continue;
                    var id = cols[0].Trim();
                    var points = cols[1].Split(',');
                    var angle = -1.0;
                    if (cols.Length > 2)
                        angle = double.Parse(cols[2]);
                    var pt = new Point { X = int.Parse(points[0].Trim()), Y = int.Parse(points[1].Trim()), Angle = angle};
                    _senario.Add(new Senario { Id = int.Parse(id), Point = pt});
                }
            }
        }

        public IEnumerator<Senario> GetEnumerator()
        {
            return _senario.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override SenarioVersion Version { get { return SenarioVersion.V2; } }
    }
}