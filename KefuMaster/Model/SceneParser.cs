﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KefuMaster
{
    public class SceneParser : IEnumerable<SceneParser.Scene>
    {
        public class Scene
        {
            public string Label { get; set; }
            public string File { get; set; }
        }

        private readonly List<Scene> _scenes;
        private int _current;

        public SceneParser(string fileName)
        {
            _scenes = new List<Scene>();
            using(var fs = new FileStream(fileName, FileMode.Open))
            using (var sr = new StreamReader(fs))
            {
                string s = null;
                while ((s = sr.ReadLine()) != null)
                {
                    var names = s.Split('#').ToArray();
                    var file = names[0].Trim();
                    var label = file;
                    if (names.Length > 1)
                        label = names[1].Trim();
                    _scenes.Add(new Scene { Label = label, File = file });
                }
            }
        }

        bool MoveNext() { if (_current == _scenes.Count) return false; _current++; return true; }
        bool MovePrev() { if (_current == 0) return false; _current--; return true; }

        public SenarioParser Current { get { return SenarioParser.Parse(_scenes[_current].File); } }

        public SenarioParser this[int index]{
            get
            {
                return SenarioParser.Parse(_scenes[index].File);
            }
        }

        public IEnumerator<SceneParser.Scene> GetEnumerator()
        {
            return _scenes.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
